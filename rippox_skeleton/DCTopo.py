#!/usr/bin/python

'''
Fat tree topology for data center networking

 based on riplpox 
'''
from mininet.topo import Topo
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call

from mininet.topo import Topo

class FatTreeTopo( Topo ):
    #"Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        print( '*** Add Core switches\n')

   
        #*************************** Add Core Switches ***************************
        ##addressing 10.k.j,i
        s1 = self.addSwitch('s1', cls=OVSKernelSwitch,ip='10.4.1.1')
	#print("\n----------------printing from DCTOPO S1.ip = "+s1+"\n\n")
        s2 = self.addSwitch('s2', cls=OVSKernelSwitch,ip='10.4.1.2')
        s3 = self.addSwitch('s3', cls=OVSKernelSwitch,ip='10.4.2.1')
        s4 = self.addSwitch('s4', cls=OVSKernelSwitch,ip='10.4.2.2')

        #*************************** Add Pod Switches ***************************
       
        print( '*** Add Pods switches\n')
        #===================================
        #==========    Pod 0  ==============
        #===================================
        ##addressing 10.pod.switch.1

        print( '------- Add Pod 0\n')
        #aggregations
        s5 = self.addSwitch('s5', cls=OVSKernelSwitch,ip='10.0.2.1')
        s6 = self.addSwitch('s6', cls=OVSKernelSwitch,ip='10.0.3.1')

        #edges
        s7 = self.addSwitch('s7', cls=OVSKernelSwitch,ip='10.0.0.1')
        s8 = self.addSwitch('s8', cls=OVSKernelSwitch,ip='10.0.1.1')
        
        #===================================
        #==========    Pod 1  ==============
        #===================================

        print( '------- Add Pod 1\n')
        #aggregations
        s9 = self.addSwitch('s9', cls=OVSKernelSwitch,ip='10.1.2.1')
        s11 = self.addSwitch('s11', cls=OVSKernelSwitch,ip='10.1.3.1')

        #edges
        s10 = self.addSwitch('s10', cls=OVSKernelSwitch,ip='10.1.0.1')
        s12 = self.addSwitch('s12', cls=OVSKernelSwitch,ip='10.1.1.1')

        #===================================
        #==========    Pod 2  ==============
        #===================================
        
        print( '------- Add Pod 2\n')
        #aggregations
        s13 = self.addSwitch('s13', cls=OVSKernelSwitch,ip='10.2.2.1')
        s15 = self.addSwitch('s15', cls=OVSKernelSwitch,ip='10.2.3.1')

        #edges
        s14 = self.addSwitch('s14', cls=OVSKernelSwitch,ip='10.2.0.1')
        s16 = self.addSwitch('s16', cls=OVSKernelSwitch,ip='10.2.1.1')


        #===================================
        #==========    Pod 3  ==============
        #===================================

        print( '------- Add Pod 3\n')
        #aggregations
        s17 = self.addSwitch('s17', cls=OVSKernelSwitch,ip='10.3.2.1')
        s19 = self.addSwitch('s19', cls=OVSKernelSwitch,ip='10.3.3.1')
      
        #edges
        s18 = self.addSwitch('s18', cls=OVSKernelSwitch,ip='10.3.0.1')
        s20 = self.addSwitch('s20', cls=OVSKernelSwitch,ip='10.3.1.1')
        
       
        #*************************** Add Pod Hosts ***************************

        print( '*** Add hosts\n')

        #connected to edge pod0
        h1 = self.addHost('h1', cls=Host, ip='10.0.0.2', defaultRoute=None)
        h2 = self.addHost('h2', cls=Host, ip='10.0.0.3', defaultRoute=None)

        h3 = self.addHost('h3', cls=Host, ip='10.0.1.2', defaultRoute=None)
        h4 = self.addHost('h4', cls=Host, ip='10.0.1.3', defaultRoute=None)

        #connected to edge pod1
        h5 = self.addHost('h5', cls=Host, ip='10.1.0.2', defaultRoute=None)
        h6 = self.addHost('h6', cls=Host, ip='10.1.0.3', defaultRoute=None)

        h7 = self.addHost('h7', cls=Host, ip='10.1.1.2', defaultRoute=None)
        h8 = self.addHost('h8', cls=Host, ip='10.1.1.3', defaultRoute=None)

        #connected to edge pod2
        h9 = self.addHost('h9', cls=Host, ip='10.2.0.2', defaultRoute=None)
        h10 = self.addHost('h10', cls=Host, ip='10.2.0.3', defaultRoute=None)

        h11 = self.addHost('h11', cls=Host, ip='10.2.1.2', defaultRoute=None)
        h12 = self.addHost('h12', cls=Host, ip='10.2.1.3', defaultRoute=None)
        
        #connected to edge pod3
        h13 = self.addHost('h13', cls=Host, ip='10.2.0.2', defaultRoute=None)
        h14 = self.addHost('h14', cls=Host, ip='10.2.0.3', defaultRoute=None)
        
        h15 = self.addHost('h15', cls=Host, ip='10.2.1.2', defaultRoute=None)
        h16 = self.addHost('h16', cls=Host, ip='10.2.1.3', defaultRoute=None)
        
        
        
        
        

        info( '*** Add links\n')
        self.addLink(h1, s7)
        self.addLink(s7, h2)
        self.addLink(h3, s8)
        self.addLink(s8, h4)
        self.addLink(s8, s6)
        self.addLink(s6, s7)
        self.addLink(s7, s5)
        self.addLink(s5, s8)
        self.addLink(s5, s1)
        self.addLink(s1, s9)
        self.addLink(s1, s13)
        self.addLink(s1, s17)
        self.addLink(h5, s10)
        self.addLink(s10, h6)
        self.addLink(s10, s9)
        self.addLink(s10, s11)
        self.addLink(s11, s12)
        self.addLink(s12, s9)
        self.addLink(h7, s12)
        self.addLink(s12, h8)
        self.addLink(s14, s13)
        self.addLink(s13, s16)
        self.addLink(s16, s15)
        self.addLink(s15, s14)
        self.addLink(s18, s17)
        self.addLink(s17, s20)
        self.addLink(s20, s19)
        self.addLink(s19, s18)
        self.addLink(s20, h16)
        self.addLink(s20, h15)
        self.addLink(s18, h13)
        self.addLink(s18, h14)
        self.addLink(s16, h11)
        self.addLink(s16, h12)
        self.addLink(s14, h10)
        self.addLink(s14, h9)
        self.addLink(s2, s5)
        self.addLink(s2, s9)
        self.addLink(s2, s13)
        self.addLink(s2, s17)
        self.addLink(s3, s6)
        self.addLink(s3, s11)
        self.addLink(s3, s15)
        self.addLink(s3, s19)
        self.addLink(s4, s6)
        self.addLink(s4, s11)
        self.addLink(s4, s15)
        self.addLink(s4, s19)

#we use mytopo to run it using mn command
topos = { 'FatTreeTopo': ( lambda: FatTreeTopo() ) }
#class FatTreeNode(object):
