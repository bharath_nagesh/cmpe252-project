INPUT_DIR=inputs
OUTPUT_DIR=results
INPUT_FILES='stag_prob_0_2_3_data' 
DURATION=50

for f in $INPUT_FILES;
do
        input_file=$INPUT_DIR/$f
        pref="dijkstra_results"
        out_dir=$OUTPUT_DIR/$pref/$f
        sudo python mn_ft.py -i $input_file -d $out_dir -p 0.03 -t $DURATION --dijkstra --iperf
done


# dijkstra
 
# two-level routing
