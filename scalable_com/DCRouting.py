''' 
 based on riplpox 
'''

import logging
from copy import copy



class Routing(object):
    '''Base class for data center network routing.

    Routing engines must implement the get_route() method.
    '''

    def __init__(self, topo):
        '''Create Routing object.

        @param topo Topo object from Net parent
        '''
        self.topo = topo

    def get_route(self, src, dst, hash_):
        '''Return flow path.

        @param src source host
        @param dst destination host
        @param hash_ hash value

        @return flow_path list of DPIDs to traverse (including hosts)
        '''
        raise NotImplementedError

class HashedRouting(Routing):
    ''' Hashed routing '''

    def __init__(self, topo):
        self.topo = topo

    def get_route(self, src, dst, hash_):
        ''' Return flow path. '''
        


class DijkstraRouting(Routing):
    ''' Dijkstra routing '''

    def __init__(self, topo):
        self.topo = topo

    def get_route(self, src, dst):
        ''' Return flow path. '''


        visited = {src: 0}
        path = {}

        nodes = set(self.g.nodes())

        while nodes: 
            min_node = None
            for node in nodes:
                if node in visited:
                    if min_node is None:
                        min_node = node
                    elif visited[node] < visited[min_node]:
                        min_node = node

            if min_node is None:
                break

        nodes.remove(min_node)
        current_weight = visited[min_node]

        for edge in graph.edges[min_node]:
            weight = current_weight + graph.distance[(min_node, edge)]
            if edge not in visited or weight < visited[edge]:
                visited[edge] = weight
                path[edge] = min_node
        print("****************Dijkstra returning path: ")
        print(path)
        return path


 